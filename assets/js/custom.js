// Aler popup
function success(msg) {
    var dom = '<div class="top-alert"><div class="alert alert-success alert-dismissible fade show " role="alert"><i class="fa fa-shopping-bag"></i> ' + msg + '</div></div>';
    var jdom = $(dom);
    jdom.hide();
    $("body").append(jdom);
    jdom.fadeIn();
    setTimeout(function() {
        jdom.fadeOut(function() {
            jdom.remove();
        });
    }, 2000);
}

function error(msg) {
    var dom = '<div class="top-alert"><div class="alert alert-danger alert-dismissible fade in " role="alert"><i class="glyphicon glyphicon-exclamation-sign"></i> ' + msg + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div></div>';
    var jdom = $(dom);
    jdom.hide();
    $("body").append(jdom);
    jdom.fadeIn();
    setTimeout(function() {
        jdom.fadeOut(function() {
            jdom.remove();
        });
    }, 2000);
}

function warning(msg) {
    var dom = '<div class="top-alert"><div class="alert alert-warning alert-dismissible fade in " role="alert"><i class="glyphicon glyphicon-question-sign"></i> ' + msg + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button></div></div>';
    var jdom = $(dom);
    jdom.hide();
    $("body").append(jdom);
    jdom.fadeIn();
    setTimeout(function() {
        jdom.fadeOut(function() {
            jdom.remove();
        });
    }, 2000);
}

function info(msg) {
    var dom = '<div class="top-alert"><div class="alert alert-info alert-dismissible fade in " role="alert"><i class="glyphicon glyphicon-info-sign"></i> ' + msg + '</div></div>';
    var jdom = $(dom);
    jdom.hide();
    $("body").append(jdom);
    jdom.fadeIn();
    setTimeout(function() {
        jdom.fadeOut(function() {
            jdom.remove();
        });
    }, 2000);
}

$(document).ready(function() {

    /*-----floating input field start-----*/
    $('.input-control input, .input-control textarea').on('focus', function() {
        if (!$(this).hasClass('input--focused')) {
            $(this).addClass('input--focused');
        }
    }).on('blur', function() {
        if ($(this).val() === '' && $(this).hasClass('input--focused')) {
            $(this).removeClass('input--focused')
        }
    })
    /*-----floating input field end-----*/

    $('[data-toggle="popover"]').popover();
    $(".toggleClick").on('click', function(e) {
        $('.contactus-container').toggleClass("move-box");
        $('#overlay').toggleClass("move-overlay");
        e.preventDefault();
    })

    $('.navbar .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(150).slideDown();
    }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
    });

    var categorySlider = new Swiper('.categorySlider', {
        slidesPerView: 4,
        spaceBetween: 0,
        navigation: {
            nextEl: '.button-next',
            prevEl: '.button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
            },
            640: {
                slidesPerView: 1.5,
            },
            320: {
                slidesPerView: 1,
            }
        }
    });

    var testimonial = new Swiper('.testimonial', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: true,
        navigation: {
            nextEl: '.test-button-next',
            prevEl: '.test-button-prev',
        }
    });



    $("#user-profile .nav-link").on("click", function() {
        var curId = $(this).attr("href");
        // alert(curId);
        $(".tab-pane").removeClass("active show");
        $(".nav-justified .nav-link").removeClass("active");
        $(".tab-pane" + curId).addClass("active show");
    });

    $("#user-profile .myorder-pane").on("click", function() {
        $(".order-details").removeClass("active show");
        $(".myorder").addClass("active show");

    });
    $('#cont').find('span').click(function() {
        if ($(this).hasClass('first')) {
            $('#progress-bar').val('0');
            $(this).nextAll().removeClass('border-change');
        } else if ($(this).hasClass('second')) {
            $(this).nextAll().removeClass('border-change');
            $('#progress-bar').val('50');
            $(this).prevAll().addClass('border-change');
            $(this).addClass('border-change');
        } else {
            $('#progress-bar').val('100');
            $(this).addClass('border-change');
            $(this).prevAll().addClass('border-change');
        }
    });

    $('.nextbtn').click(function(e) {
        e.preventDefault();
        $('.cart-tabs button[href="#shipping"]').tab('show');
    });
    $('.back-to-cart').click(function(e) {
        e.preventDefault();
        $('.cart-tabs button[href="#cart"]').tab('show');
    });
    $('.continue-to-payment').click(function(e) {
        e.preventDefault();
        $('.cart-tabs button[href="#payment"]').tab('show');
    });
    /*!
     * jQuery UI Touch Punch 0.2.3
     *
     * Copyright 2011–2014, Dave Furfero
     * Dual licensed under the MIT or GPL Version 2 licenses.
     *
     * Depends:
     *  jquery.ui.widget.js
     *  jquery.ui.mouse.js
     */
    ! function(a) {
        function f(a, b) {
            if (!(a.originalEvent.touches.length > 1)) {
                a.preventDefault();
                var c = a.originalEvent.changedTouches[0],
                    d = document.createEvent("MouseEvents");
                d.initMouseEvent(b, !0, !0, window, 1, c.screenX, c.screenY, c.clientX, c.clientY, !1, !1, !1, !1, 0, null), a.target.dispatchEvent(d)
            }
        }
        if (a.support.touch = "ontouchend" in document, a.support.touch) {
            var e, b = a.ui.mouse.prototype,
                c = b._mouseInit,
                d = b._mouseDestroy;
            b._touchStart = function(a) { var b = this;!e && b._mouseCapture(a.originalEvent.changedTouches[0]) && (e = !0, b._touchMoved = !1, f(a, "mouseover"), f(a, "mousemove"), f(a, "mousedown")) }, b._touchMove = function(a) { e && (this._touchMoved = !0, f(a, "mousemove")) }, b._touchEnd = function(a) { e && (f(a, "mouseup"), f(a, "mouseout"), this._touchMoved || f(a, "click"), e = !1) }, b._mouseInit = function() {
                var b = this;
                b.element.bind({ touchstart: a.proxy(b, "_touchStart"), touchmove: a.proxy(b, "_touchMove"), touchend: a.proxy(b, "_touchEnd") }), c.call(b)
            }, b._mouseDestroy = function() {
                var b = this;
                b.element.unbind({ touchstart: a.proxy(b, "_touchStart"), touchmove: a.proxy(b, "_touchMove"), touchend: a.proxy(b, "_touchEnd") }), d.call(b)
            }
        }
    }(jQuery);


    var idName = '#price-range-slider';
    if ($(idName).length == 1) {

        $(idName).draggable();
    }

    // When the user scrolls the page, execute myFunction 
    window.onscroll = function() { myFunction() };

    // Get the header
    var header = document.getElementById("stickyheader");

    // Get the offset position of the navbar
    var sticky = header.offsetTop;

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
        if (window.pageYOffset > '120') {
            header.classList.add("sticky-header");
        } else {
            header.classList.remove("sticky-header");
        }
    }



});

(function($) {
    //Function to animate slider captions 
    function doAnimations(elems) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';

        elems.each(function() {
            var $this = $(this),
                $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function() {
                $this.removeClass($animationType);
            });
        });
    }

    //Variables on page load 
    var $myCarousel = $('#bannerSlider'),
        $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");

    //Initialize carousel 
    $myCarousel.carousel();

    //Animate captions in first slide on page load 
    doAnimations($firstAnimatingElems);

    //Pause carousel  
    $myCarousel.carousel('pause');


    //Other slides to be animated on carousel slide event 
    $myCarousel.on('slide.bs.carousel', function(e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });




    // $(".cart-tabs").find("li a.active").prevAll("li").addClass("visited");



})(jQuery);